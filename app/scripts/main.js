  var width = $(window).width(),
    hash = window.location.hash, 
    height = $(window).height();

  $(document).ready(function() {


    $('.checkout-panel__top > ul > li > a').click(function() {
      var step = $(this).attr('href');
      if(step != '#'){
      $('.checkout-panel__top > ul > li').removeClass('active');
        $(this).parent().addClass('active');
        $('.checkout-panel__body').removeClass('hidden');
        $('.checkout-panel__body').hide();
        $(step).show();
      }
      return false;
    });

    $('#go-to-2-step').click(function(){
      $('.checkout-panel__body').removeClass('hidden');
      $('.checkout-panel__body').hide();
      $('#checkout-step-2').show();
      $('.checkout-panel__top > ul > li').removeClass('active');
      $('.checkout-panel__top > ul > li > a[href="#checkout-step-2"]').parent().addClass('active');
      return false;    
    });


    $('.product-block__gallery__main a').colorbox();

    //lets find timers and init them
    var timers = $('.product-preview__head__timer');
    if (timers.length > 0) {
      setTimeout(function() {
        timers.each(function(index) {
          var date = $(this).data('date');
          if (date != undefined) {
            var fomratData = Date.parse(date);
            $(this).countdown(fomratData, function(event) {
              $(this).html(event.strftime('%Dd %H:%M:%S'));
            });
          }
        });
      }, 100);

    }

    var productTimer = $('.product-block__timer');
    if (productTimer.length > 0) {
      productTimer.each(function(index) {
        var date = $(this).data('expire');
        if (date != undefined) {
          var fomratData = Date.parse(date);
          $(this).countdown(fomratData, function(event) {
            $(this).find('.days > span').html(event.strftime('%D'));
            $(this).find('.hourses > span').html(event.strftime('%H'));
            $(this).find('.minutes > span').html(event.strftime('%M'));
            $(this).find('.seconds > span').html(event.strftime('%S'));
          });
        }
      });
    }

    // finding products slider and init them all
    var productSliders = $('.product-slider');
    if (productSliders.length > 0) {
      productSliders.slick({
        dots: true,
        arrows: true,
        infinite: false,
        speed: 500,
        accessibility: false,
        rows: 2,
        appendDots: $('.block-title__slider-controls > .dots'),
        prevArrow: $('.block-title__slider-controls .arrows > .sl-prev'),
        nextArrow: $('.block-title__slider-controls .arrows > .sl-next'),
        slidesPerRow: 4,
        responsive: [{
            breakpoint: 480,
            settings: {
              rows: 2,
              slidesPerRow: 1
            }
          },
          {
            breakpoint: 767,
            settings: {
              rows: 2,
              slidesPerRow: 2
            }
          },
          {
            breakpoint: 991,
            settings: {
              rows: 2,
              slidesPerRow: 3
            }
          }
        ]
      });
    }

    $('.sl-next').click(function() {
      return false;
    });

    $('a[href="#load-cats"]').click(function() {
      console.log('sad');
      var catMenu = $('div.categories-list__nav > ul');
      if ($(this).parent().hasClass('active')) {
        $(this).find('span').html('Daugiau kategorijų');
        $(this).parent().removeClass('active');
        $($(this).parent()).insertAfter('div.categories-list__nav > ul > li:nth-child(6)');
        catMenu.find('li').each(function(index) {
          if (index > 6) {
            $(this).addClass('hidden');
          }
        });
      } else {
        $(this).find('span').html('Mažiau kategorijų');
        $(this).parent().addClass('active');
        $($(this).parent()).insertAfter('div.categories-list__nav > ul > li:last-child');
        catMenu.find('li').each(function(index) {
          if (index > 6) {
            $(this).removeClass('hidden');
          }
        });
      }
    });

    //init selects 
    $('.prik-select').selectpicker({
      style: 'prik-select__block',
      size: 4
    });

    // for mobile menu(m-menu)
    $('.m-menu__categories-list__nav > li > a').click(function() {
      if ($(this).parent('li').hasClass('active')) {
        $(this).parent().removeClass('active');
      } else {
        $('.m-menu__categories-list__nav > li').removeClass('active');
        $(this).parent().addClass('active');
      }

      return false;
    });

    $('.m-menu__body__categories-btn').click(function() {
      $('.m-menu__body, .m-menu__footer').addClass('hidden');
      $('.m-menu__categories-list').fadeIn();
    });

    $('.m-menu__categories-list_back').click(function() {
      $('.m-menu__body, .m-menu__footer').removeClass('hidden');
      $('.m-menu__categories-list').fadeOut();
    });

    $('.menu-btn').click(function() {
      $(this).children('i').toggleClass('icon-times');
      $('.m-menu').fadeToggle('100');
    });

    var productGallery = $('.product-block__gallery');
    if (productGallery.length > 0) {
      var navSlider = $('.product-block__gallery__nav');
      var slider = $('.product-block__gallery__main');
      slider.slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        dots: true,
        fade: true,
        appendDots: $('.block-title__slider-controls > .dots'),
        prevArrow: $('.block-title__slider-controls .arrows > .sl-prev'),
        nextArrow: $('.block-title__slider-controls .arrows > .sl-next'),
        asNavFor: navSlider
      });
      navSlider.slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        asNavFor: slider,
        dots: false,
        centerMode: false,
        arrows: true,
        focusOnSelect: true,
        responsive: [{
          breakpoint: 991,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            variableWidth: true
          }
        }]
      });
    }

    // binds widget
    $('#show-more-binds').click(function() {
      if ($(this).hasClass('btn-up')) {
        $('.widget__last-binds__item').each(function(index) {
          if (index > 4) {
            $(this).addClass('hidden');
          }
        });
        $(this).removeClass('btn-up');
      } else {
        $('.widget__last-binds__list > div').removeClass('hidden');
        $(this).addClass('btn-up');
      }
    });

    var categoriesToggle = $('.categories-btn');
    var offsets = categoriesToggle.offset();
    var catMenu = $('.categories-menu');

    if (categoriesToggle.length > 0) {
      //catMenu.css('top', offsets.top + categoriesToggle.outerHeight());
      catMenu.css('top', $('.header').height() + $('.search-block').height() + 15);
      catMenu.css('right', width - offsets.left - categoriesToggle.outerWidth() +1);
      console.log(offsets.left);
    }

    // large categories menu hiding/showing
    $('.categories-btn, .categories-menu').mouseover(function() {
      $('.categories-menu').show();
      $('.categories-btn').addClass('active');
    }).mouseleave(function() {
      $('.categories-menu').hide();
      $('.categories-btn').removeClass('active');
      setInterval(function() {
        $('.categories-menu').mouseover(function() {
          $('.categories-menu').show();
          $('.categories-btn').addClass('active');
        });
      }, 100);
    });

    // focus serach input animation
    $('#typewritten-placeholder').focus(function() {
      var placeString = $(this).attr('placeholder');
      $(this).attr('placeholder', '');
      var letterIndex = 0;
      setInterval(function() {
        if (placeString.length == letterIndex) {} else {
          addTextToPlaceholder(placeString, letterIndex);
          letterIndex = letterIndex + 1;
        }
      }, 50);
    });






    // add more params auction creating form
    $('button#add-more-params').click(function() {
      var paramsBlock = $('#parametrs');
      var example = $('#example-of-param-block');
      var template = `
                            <div class="add-auction__paramets-block">
                              <div class="col-md-6 col-sm-6">
                                <input type="text" class="form-control pirk-control" placeholder="pvz. Dydis">
                              </div>
                              <div class="col-md-6 col-sm-6">
                                <input type="text" class="form-control pirk-control" placeholder="pvz. 60 x 80 cm">
                              </div>
                            </div>
          `;
      paramsBlock.append(template);
    });

    $('#add-auction-category').change(function() {
      $('select[name="sub_category"], select[name="sub_sub_category"]').removeAttr('disabled');
    });

    $('#step-1-form').submit(function() {
      //prepere first step
      $('#step-1-form #step-1').collapse('hide');

      // prepere second step
      $('#step-2-form .panel-step').removeClass('step-disabled');
      $('#step-2-form #collapse2').collapse('show');
      $('#step-2-form .panel-step').addClass('step-active');
      return false;
    });

    $('#step-2-form').submit(function() {
      //prepere first step
      $('#step-2-form #collapse2').collapse('hide');

      // prepere second step
      $('#step-3-form .panel-step').removeClass('step-disabled');
      $('#step-3-form #collapse3').collapse('show');
      $('#step-3-form .panel-step').addClass('step-active');
      return false;
    });
    //collapse steps
    $('.collapse').on('show.bs.collapse', function() {
      if ($(this).parent().hasClass('step-disabled')) {
        return false;
        $('#' + $(this).attr('id')).collapse('hide');
      }
    });
    //steps collapse when close
    $('.collapse').on('hide.bs.collapse', function() {
      if ($(this).parent().hasClass('step-opened')) {
        $(this).parent().removeClass('step-opened')
      }
    });

    // check boxes with disabled inputs
    $('#checkboxs-list-has-disabled input[type="checkbox"]').change(function() {
      var input = $(this).parent().parent().parent().find('input.form-control');
      if (input != undefined && input.length > 0) {
        if (input.attr('disabled') == 'disabled') {
          input.removeAttr('disabled');
        } else {
          input.val('');
          input.attr('disabled', 'disabled');
        }
      }
    });

    $('#checkboxs-list-has-disabled input[type="text"]').keyup(function() {
      var checkbox = $(this).parent().parent().parent().find('input[type="checkbox"]');
      if (checkbox.attr('checked') == 'checked' && $(this).val().length == 0) {
        checkbox.removeAttr('checked');
      } else {
        if ($(this).val().length > 0) {
          checkbox.attr('checked', 'checked');
        } else {
          checkbox.removeAttr('checked');
        }
      }

    });

    $('.reg-btn[data-target="#sign-in-modal"]').click(function() {
      $('a[href="#sign-up-tab"]').tab('show');
    });

    $('.show-forgot-tab').click(function() {
      $('a[href="#forgot-password"]').tab('show');
      $('a[href="#sign-in-tab"]').parent().addClass('active-my');
    });
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
      $('a[data-toggle="tab"]').parent().removeClass('active-my');
    })
  });

  // checkout boxes auth 
  $('#checkout-auth-boxes input[type="radio"]').change(function() {
    var radioID = $(this).attr('id');
    if (radioID == 'rad-1') {
      $('#sign-in-form').removeClass('hidden');
      $('#sign-up-form').addClass('hidden');
    } else {
      $('#sign-in-form').addClass('hidden');
      $('#sign-up-form').removeClass('hidden');
    }
  });

  // delivery  checkbox
  $('.radioboxes-list__delivery input[type="radio"]').change(function() {

    var block = $(this).data('form');
    $('.checkout-form-group').fadeOut(100);
    if ($(block).css('display') == 'none') {
      $(block).fadeIn(300);
    }
    return false;
  });

  // payemnts
  $('.payments-list__method').click(function(){
    $('.payments-list__method').removeClass('active');
    $(this).addClass('active');
    var paymentID = $(this).data('method');
    $('.payment-method-details').fadeOut(100);
    $(paymentID).fadeIn(300);
  });

  // popoups
  $('.what-is').mouseover(function(){
    var popupID = $(this).data('popup');
    var offsets = $(this).offset();
    var elemWidth = $(this).outerWidth();
    var elemHeight = $(this).outerHeight();
    $(popupID).css({
      top: offsets.top-$(popupID).outerHeight(),
      left: offsets.left-$(popupID).outerWidth()+elemWidth
    });
  }).mouseleave(function(){
    var popupID = $(this).data('popup');
    $(popupID).css({
      left: -9999999999
    });
  });


  // checkout steps
  $('.checkout-panel__top > ul > li > a').click(function(){
    var tabID = $(this).attr('step');

  });

  $(window).resize(function() {
    var categoriesToggle = $('.categories-btn');
    if (categoriesToggle.length > 0) {
      var catMenu = $('.categories-menu');
      categoriesToggle.removeClass('active');
      catMenu.hide();
    }
    $('#m-menu').hide();
    $('.menu-btn i').removeClass('icon-times');
    //lets find timers and init them
    var timers = $('.product-preview__head__timer');
    if (timers.length > 0) {
      setTimeout(function() {
        timers.each(function(index) {
          var date = $(this).data('date');
          if (date != undefined) {
            var fomratData = Date.parse(date);
            $(this).countdown(fomratData, function(event) {
              $(this).html(event.strftime('%Dd %H:%M:%S'));
            });
          }
        });
      }, 100);

    }
  });

  function addTextToPlaceholder(placeString, letterIndex) {
    var text = placeString.split('');
    $('#typewritten-placeholder').attr('placeholder', $('#typewritten-placeholder').attr('placeholder') + text[letterIndex]);
  }

  document.addEventListener('DOMContentLoaded', function(event) {
    // array with texts to type in typewriter
    var dataText = ['Pirk ir parduok daiktus lengvai!', 'Hello world', 'How are you?'];

    // type one text in the typwriter
    // keeps calling itself until the text is finished
    function typeWriter(text, i, fnCallback) {
      // chekc if text isn't finished yet
      if (i < (text.length)) {
        // add next character to h1
        if (document.querySelector('#typewriter') != null) {
          if (document.querySelector('#typewriter').placeholder == undefined) {
            document.querySelector('#typewriter').innerHTML = text.substring(0, i + 1) + '<span aria-hidden="true"></span>';
          } else {
            document.querySelector('#typewriter').placeholder = text.substring(0, i + 1);
          }
        }


        // wait for a while and call this function again for next character
        setTimeout(function() {
          typeWriter(text, i + 1, fnCallback)
        }, 100);
      }
      // text finished, call callback if there is a callback function
      else if (typeof fnCallback == 'function') {
        // call callback after timeout
        setTimeout(fnCallback, 700);
      }
    }
    // start a typewriter animation for a text in the dataText array
    function StartTextAnimation(i) {
      if (typeof dataText[i] == 'undefined') {
        setTimeout(function() {
          StartTextAnimation(0);
        }, 20000);
      }
      // check if dataText[i] exists
      if (dataText[i] != undefined) {
        if (i < dataText[i].length) {
          // text exists! start typewriter animation
          typeWriter(dataText[i], 0, function() {
            // after callback (and whole text has been animated), start next text
            StartTextAnimation(i + 1);
          });
        }
      }

    }
    // start the text animation
    StartTextAnimation(0);
  });
